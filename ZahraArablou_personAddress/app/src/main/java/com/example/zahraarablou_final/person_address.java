package com.example.zahraarablou_final;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

public class person_address extends AppCompatActivity implements View.OnClickListener {
   Person selectedPerson;
    TextView personName;
    EditText city,street,houseNo;
    Button btnSave;
    int selectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_address);
        getIntentFromMain();
        initialize();
    }
    private void getIntentFromMain(){
        selectedPerson=(Person)getIntent().getBundleExtra("extraIntent").getSerializable("personBundle");
        selectedPosition=(int)getIntent().getBundleExtra("extraIntent").getInt("selectedPosition");
    }
    private void initialize(){
        personName=findViewById(R.id.personName);
        city=findViewById(R.id.city);
        street=findViewById(R.id.street);
        houseNo=findViewById(R.id.houseNo);
if(selectedPerson!=null){
    personName.setTextKeepState(selectedPerson.getName());
    city.setText(selectedPerson.getAddress().getCity());
    street.setText(selectedPerson.getAddress().getStreet());
    houseNo.setText(selectedPerson.getAddress().getHouse_no());
}
        btnSave=findViewById(R.id.save);
        btnSave.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

      Person person=new Person(personName.getText().toString(),new Address(city.getText().toString(),street.getText().toString(),houseNo.getText().toString()));
      Bundle bundle=new Bundle();
      System.out.println(person);
      bundle.putSerializable("personObjectEdited",person);
      bundle.putInt("selectedPosition",selectedPosition);

      Intent returnIntent=new Intent();
      returnIntent.putExtra("intentReturn",bundle);
      setResult(RESULT_OK,returnIntent);

     finish();

    }
}