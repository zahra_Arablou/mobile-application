package com.example.zahraarablou_final;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.content.Intent;
import android.widget.Toast;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, View.OnClickListener {
     Button btnCreate;
    ListView personListView;
    List<Person> personList;
    ArrayAdapter<Person> arrayAdapter;
    static final int OBJ_SEND_REQ=1;
    static final int ADD_SEND_REQ=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDummyPerson();
        initializeViewItem();
    }
    private void initializeDummyPerson(){

        personList=new ArrayList<Person>();
        personList.add(new Person("Jerry",new Address("Montreal","Kildare","200")));
        personList.add(new Person("Kevin",new Address("Toronto","Rochelle","15")));
        personList.add(new Person("Mery",new Address("Quebec City","Omid","567")));

    }
    private void initializeViewItem(){
        personListView=findViewById(R.id.listViewPerson);
        personListView.setOnItemClickListener(this);
        personListView.setOnItemLongClickListener(this);

        btnCreate=findViewById(R.id.create);
        btnCreate.setOnClickListener(this);

        arrayAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,personList);
        personListView.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
           Bundle bundle=new Bundle();
           bundle.putSerializable("personBundle",(Serializable) personList.get(position));
           bundle.putInt("SelectedPositon",position);


            Intent myIntent=new Intent(this,person_address.class);
            myIntent.putExtra("extraIntent",bundle);

           startActivityForResult(myIntent,OBJ_SEND_REQ);

    }
      @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

          AlertDialog.Builder builder=new AlertDialog.Builder(this);
          builder.setTitle("Delete Confirmation!");
          builder.setMessage("Are you sure to delete?");
          builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                  personList.remove(position);
                  arrayAdapter.notifyDataSetChanged();
              }
          });

          builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
          });

          AlertDialog dialog=builder.create();
          dialog.show();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==OBJ_SEND_REQ){
        if(resultCode == RESULT_OK){

           Person person=(Person)data.getBundleExtra("intentReturn").getSerializable("personObjectEdited");
           int selectedPosition=(int)data.getBundleExtra("intentReturn").getInt("selectedPosition");

            personList.set(selectedPosition,person);

            Toast.makeText(this, person.getAddress().getStreet(), Toast.LENGTH_LONG).show();
            System.out.println("***********************"+person);

            arrayAdapter.notifyDataSetChanged();
            }

        }

        if(requestCode==ADD_SEND_REQ){
            if(resultCode == RESULT_OK){
                String recivedDate=(String)data.getStringExtra("return_result");
                Toast.makeText(this, recivedDate, Toast.LENGTH_LONG).show();
               // arrayAdapter.notifyDataSetChanged();
            }
        }

    }


    @Override
    public void onClick(View v) {
        Intent intent=new Intent(this,Add.class);
        startActivityForResult(intent,ADD_SEND_REQ);
    }
}