package com.example.web;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //request for permission in manifest
WebView webView;
Button btnLoad;
String url="http://www.yahoo.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    private void initialize(){
        webView =findViewById(R.id.webView);
        btnLoad=findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        //set webView element as out browser or webViewClient,
        //so web pages will be display in same layout instead of a browser
        webView.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}