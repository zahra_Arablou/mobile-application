package com.example.starbuzz_coffe;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.content.Intent;

public class MainActivity extends Activity {
ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }
    private void initialize(){
        //1-Create a list view with XML array entries
        listView=findViewById(R.id.list_options);
        //1-1- listView Adapter is set in listView XML by an array from strings.xml
        //2-Create an OnItemClickListener
        AdapterView.OnItemClickListener itemClickListener=
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> listView,
                                            View itemView,
                                            int position, //Position in the listview start from zero
                                            long id ) {// Row id of the underlying data
                        if(position==0){
                            Intent intent=new Intent(MainActivity.this,category.class);
                            startActivity(intent);
                        }

                    }
                };
                //3-Add the listener to the list view
        listView.setOnItemClickListener(itemClickListener);
    }
}