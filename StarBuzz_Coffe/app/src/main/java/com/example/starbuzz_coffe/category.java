package com.example.starbuzz_coffe;

import androidx.appcompat.app.AppCompatActivity;
import com.example.starbuzz_coffe.model.Drink;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.content.Intent;

public class category extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initialize();
    }
    private void initialize(){
        //1- Initialize listView
        ListView listDrinks=findViewById(R.id.list_drinks);
        //2- Create an adapter for listview
        ArrayAdapter<Drink> listAdapter=new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                Drink.drinks
        );
        //3- Assign the adapter to the list view
        listDrinks.setAdapter(listAdapter);
        //4-Create a listener
        AdapterView.OnItemClickListener itemClickListener=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> linkDrinkAdapterView,
                                    View itemView,
                                    int position, //Position in the listview start from zero
                                    long id) { //Row id of the undrlying data
                //How to get item based on clicked cell from AdapterView
                System.out.println("DrinkCatagoryActivity-----------------\n"+ linkDrinkAdapterView.getItemAtPosition(position).toString());

                Toast.makeText(category.this,"Position = "+
                        position+", id = "+id , Toast.LENGTH_SHORT).show();
                //3-1- Pass the drink the user clicks on to DrinkActivity
                Intent intent=new Intent(category.this,drink.class);
                intent.putExtra(drink.EXTRA_DRINKID ,(int) id);
                startActivity (intent);

            }
        };
        //5- Assign the listenner to the list view
            listDrinks.setOnItemClickListener(itemClickListener);



}
    }