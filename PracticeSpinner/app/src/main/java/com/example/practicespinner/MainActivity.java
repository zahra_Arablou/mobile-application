package com.example.practicespinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
Spinner spinner;
Button generate,validate;
EditText answer;
TextView title;

String selectedSpinnerText;
int result,userAnswer;

String listOperation[]=new String[]{"Addition","Substraction","Multiplication","Division"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

    }
    private void initialize(){
        spinner=findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        title=findViewById(R.id.title);

//        //Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,listOperation);
//
//        //Drop dowm layout style
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        //attaching data adapter to spinner
//        spinner.setAdapter(dataAdapter);


        //or:
        ArrayAdapter<CharSequence> dataAdapter;
        dataAdapter=ArrayAdapter.createFromResource(this,R.array.spinner_values,android.R.layout.simple_spinner_item);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //TextView spinnerText=(TextView) View;
       // selectedSpinnerText=spinnerText.getText().toString();
       selectedSpinnerText=listOperation[position];
       Toast.makeText(this,"I selected " + selectedSpinnerText, Toast.LENGTH_SHORT).show();
     }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}