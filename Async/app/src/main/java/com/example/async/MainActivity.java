package com.example.async;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;

public class MainActivity extends AppCompatActivity {
TextView finalResult;
EditText inputTime;
Button buttonRun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }
    private void initialize() {
        inputTime = findViewById(R.id.sleep);
        buttonRun = findViewById(R.id.async);
        finalResult = findViewById(R.id.textViewResult);
        buttonRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sleepTime = (String) inputTime.getText().toString();
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(sleepTime);
            }
        });}


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            String startText="Sleeping process started";
            finalResult.setText(startText);
        }
        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping for "+params[0]+" secs");// Calls onProgressUpdate()
            String resp;
            try {
                int time = Integer.parseInt(params[0]) * 1000; //converting to milliseconds
                Thread.sleep(time);
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            resp = "Slept for " + params[0] + " seconds";
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            finalResult.setText(result);
        }

        @Override
        protected void onProgressUpdate(String... text) {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    text[0]);
        }
    }
}