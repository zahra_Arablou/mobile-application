package com.example.returnresulttomain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Test2 extends AppCompatActivity implements View.OnClickListener {
Button test2_ok_btn,test2_cancel_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        initialize();
    }
    private void initialize(){
        test2_ok_btn=findViewById(R.id.ok);
        test2_ok_btn.setOnClickListener(this);

        test2_cancel_btn=findViewById(R.id.cancel);
        test2_cancel_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int btn=v.getId();
        switch (btn)
        {
            case R.id.ok:
                goOk();
                break;
            case R.id.cancel:
                goCancel();
                break;
        }
    }
    private void goOk(){
        Intent intent=new Intent();
        intent.putExtra("return_result_from_test2","OK from test 2");
        setResult(RESULT_OK,intent);
       finish();
    }
    private void goCancel(){
        Intent intent=new Intent();
        intent.putExtra("return_result_from_test2","Cancel from test 2");
        setResult(RESULT_CANCELED,intent);
        finish();
    }
}