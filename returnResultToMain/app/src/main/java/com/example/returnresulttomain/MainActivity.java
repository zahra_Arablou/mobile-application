package com.example.returnresulttomain;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
final static int REQUEST_CODE1=1;
final static int REQUEST_CODE2=2;
TextView textViewFeedback;
Button btnTest1,btnTest2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    private void initialize(){
        textViewFeedback=findViewById(R.id.feedback);

        btnTest1=findViewById(R.id.test1);
        btnTest1.setOnClickListener(this);

        btnTest2=findViewById(R.id.test2);
        btnTest2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
       int btnId=v.getId();
       switch (btnId){
           case R.id.test1:
               goTest1();
               break;
           case R.id.test2:
               goTest2();
               break;
       }
    }
    private void goTest1(){
        Intent intent=new Intent(this,Test1.class);
        startActivityForResult(intent,REQUEST_CODE1);
    }
    private void goTest2(){
        Intent intent=new Intent(this,Test2.class);
        startActivityForResult(intent,REQUEST_CODE2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE1){
           String recivedDate=(String)data.getStringExtra("return_result_tag");
           if(resultCode==RESULT_OK)
               //Check GPS
               //Save data to DB
               //Call and end point
               textViewFeedback.setText(recivedDate);
           else
               textViewFeedback.setText("Cancelled");
        }
        if(requestCode==REQUEST_CODE2){
            String recivedDate=(String)data.getStringExtra("return_result_from_test2");
            if(resultCode==RESULT_OK)
                textViewFeedback.setText(recivedDate);
            else
                textViewFeedback.setText("Canceled from test2");
        }
    }
}