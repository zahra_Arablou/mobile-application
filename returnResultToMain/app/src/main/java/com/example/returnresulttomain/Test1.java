package com.example.returnresulttomain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;
import android.content.Intent;

public class Test1 extends AppCompatActivity implements View.OnClickListener, TextWatcher {
TextView operation;
EditText answer;
Button generate,validate,cancel;

int rightResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);

        initialize();
    }
    private void initialize(){
        operation=findViewById(R.id.operation);
        answer=findViewById(R.id.answer);

        generate=findViewById(R.id.generate);
        generate.setOnClickListener(this);

        validate=findViewById(R.id.validate);
        validate.setOnClickListener(this);

        cancel=findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        //----------------------------------validation
        answer.addTextChangedListener(this);
        validate.setEnabled(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            int userAnswer = Integer.valueOf(answer.getText().toString());
            if (userAnswer > 18) {
                Toast toastRange = Toast.makeText(this, "The Total should be <=18", Toast.LENGTH_SHORT);
                toastRange.setGravity(Gravity.CENTER, 0, 500);
                toastRange.show();
                validate.setEnabled(false);
            } else {
                validate.setEnabled(true);
            }
        }catch (Exception e){
            Toast toastException=Toast.makeText(this,"Enter a number data type",Toast.LENGTH_SHORT);
            toastException.setGravity(Gravity.TOP|Gravity.CENTER,0,500);
            toastException.show();
        }
    }


    @Override
    public void onClick(View v) {
int btn=v.getId();
switch(btn){
    case R.id.generate:
        goGenerate();
        break;
    case R.id.validate:
        goValidate();
        break;
    case R.id.cancel:
        goCancel();
        break;
}
    }
    private void goGenerate(){
        Random random = new Random();
        int operand1 = random.nextInt(10);
        int operand2 = random.nextInt(10);
        rightResult = operand1 + operand2;

        String operation1 = operand1 + "+" + operand2 + "= ?";

        operation.setText(operation1);
    }
    private void goValidate(){
        int integerUserAnswer = Integer.parseInt(answer.getText().toString());

        String strResult;

        if (integerUserAnswer == rightResult) {
            strResult = "Right Answer!";
        } else {
            strResult = "Wrong Answer!";
    }
        //------------------------------------ Create an intent and putExtra result string
        Intent intent = new Intent();
        intent.putExtra("return_result_tag", strResult);

        //------------------------------------ Set Result for MainActivity
        setResult(RESULT_OK, intent);
        finish();
    }
        private void goCancel() {
            String strResult = "Operation canceled";

            //------------------------------------ Create an intent
            Intent intent = new Intent();
            intent.putExtra("cancel_tag", strResult);

            //------------------------------------ Set Result for MainActivity
            setResult(RESULT_CANCELED, intent);
            finish();
        }
}