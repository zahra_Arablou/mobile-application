package com.example.bundle_arraylist_student;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowResult extends AppCompatActivity {
TextView list;
ArrayList<Student> listofStudents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        initialize();
    }
    private void initialize(){
       // listofStudents=new ArrayList<>();
        list=findViewById(R.id.list);

        Bundle bundle=getIntent().getBundleExtra("intentExtra");
        Serializable bundledListOfStudents=bundle.getSerializable("bundleExtra");
        listofStudents=(ArrayList<Student>) bundledListOfStudents;
        showListOfStudents(listofStudents);
    }
    private void showListOfStudents(ArrayList<Student> listofStudents){
     String str="";
      for (Student oneStudent : listofStudents){
          str=str+oneStudent;

      }
     list.setText(str);
    }
}