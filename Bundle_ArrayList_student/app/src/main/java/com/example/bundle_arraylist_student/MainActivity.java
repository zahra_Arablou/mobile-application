package com.example.bundle_arraylist_student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
  EditText id,name,age;
Button btnClear,btnRemove,btnAdd,btnAll;
ArrayList<Student> listOfStudents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    private void initialize(){
        listOfStudents=new ArrayList<>();

        id=findViewById(R.id.id);
        name=findViewById(R.id.name);
        age=findViewById(R.id.age);

        btnClear=findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);

        btnRemove=findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);

        btnAdd=findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnAll=findViewById(R.id.btnAll);
        btnAll.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int btn=v.getId();
        switch (btn){
            case R.id.btnClear:
                goClear();
                break;
            case R.id.btnRemove:
                goRemove();
                break;
            case R.id.btnAdd:
                goAdd();
                break;
            case R.id.btnAll:
                goAll();
                break;
           }
    }

    private void goClear(){
        id.setText(null);
        name.setText(null);
        age.setText(null);
        System.out.println("**************************");
    }

    private void goRemove(){
String sId=id.getText().toString();
        Iterator<Student> iterator=listOfStudents.iterator();
        boolean find=false;
        while(iterator.hasNext()){
            Student oneStudent=iterator.next();
            if(oneStudent.getId().equals(sId)){
                iterator.remove();
                find=true;
            }
        }
        if(find)
            Toast.makeText(this,sId+"  is deleted successfully",Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this,"the student with the id "+ sId +"  doesnot exist.",Toast.LENGTH_LONG).show();
    }

    private void goAdd(){
//        int studentId=Integer.parseInt(id.getText().toString());
//        int studentAge=Integer.parseInt(age.getText().toString());
        String studentId=(String) (id.getText().toString());
        String studentName=name.getText().toString();
        String studentAge=(String)(age.getText().toString());
        Student student=new Student(studentId,studentName,studentAge);
        listOfStudents.add(student);
        Toast.makeText(this,student.getName()+" added successfully.Array size: "+
                listOfStudents.size(),
                Toast.LENGTH_SHORT).show();

               goClear();
    }

    private void goAll(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfStudents);

        Intent intent = new Intent(this, ShowResult.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);
    }
}