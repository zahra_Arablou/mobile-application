package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.view.View;

public class MainActivity extends AppCompatActivity {
EditText editText1,editText2;
RadioGroup radioGroupOperation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    private void initialize(){
        editText1=findViewById(R.id.num1);
        editText2=findViewById(R.id.num2);

        radioGroupOperation=findViewById(R.id.groupradio);
    }
    public void operate (View view){
        int btnId=view.getId();
        switch (btnId){
            case R.id.btnrsl:
                getUserInput();
                break;
            case R.id.btnfin:
                finish();
                break;
        }
    }
    private void getUserInput(){
        float operand1=Float.parseFloat(editText1.getText().toString());
        float operand2=Float.parseFloat(editText2.getText().toString());

        int checkRadioButtonId=radioGroupOperation.getCheckedRadioButtonId();
        float result=calculate(operand1,operand2,checkRadioButtonId);
        goToResultActtivity(result);
    }
    private float calculate(float operand1,float operand2,int checkRadioButtonId){
        float result=0;
        switch(checkRadioButtonId){
            case R.id.add:
                result=operand1+operand2;
                break;
            case R.id.sub:
                result=operand1-operand2;
                break;
            case R.id.div:
                result=operand1/operand2;
                break;
            case R.id.mul:
                result=operand1*operand2;
                break;
        }
        return result;
    }
    private void goToResultActtivity(float result){
        Intent intent=new Intent(this,Result.class);
        intent.putExtra("result",result);
        System.out.println("***********"+result);
        startActivity(intent);
    }
}