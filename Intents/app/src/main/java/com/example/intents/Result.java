package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.view.View;

public class Result extends AppCompatActivity {
    EditText editTextResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initialize();
        myGetIntent();
    }
    private void initialize(){
      editTextResult=findViewById(R.id.result);
    }
    private void myGetIntent(){
       float result=getIntent().getFloatExtra("result",0f);
       String strResult=String.valueOf(result);
       editTextResult.setText(strResult);
    }
    public void goBack(View view){
             finish();
    }
}