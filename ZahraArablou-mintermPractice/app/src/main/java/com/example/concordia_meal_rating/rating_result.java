package com.example.concordia_meal_rating;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ListView;
import android.content.Intent;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import com.example.concordia_meal_rating.model.MealRating;
import java.util.Iterator;
import java.util.Collections;
import java.io.Serializable;

public class rating_result extends AppCompatActivity implements View.OnClickListener {

    RadioGroup radioGrouprate;
    ListView listViewResult;
    EditText editTextName;
    Button btnBack;

    ArrayList<MealRating> listOfMealRating;
    ArrayList<MealRating> starFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_result);

        initialize();
    }

    private void initialize(){
        radioGrouprate=findViewById(R.id.radioGroupRate);

        listViewResult=findViewById(R.id.listViewResult);
        editTextName=findViewById(R.id.editTextName);

       starFilter=new ArrayList<MealRating>();

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        Bundle bundle = getIntent().getBundleExtra("intent");
        Serializable bundlelistOfMealRating = bundle.getSerializable("bundle");

        listOfMealRating = (ArrayList<MealRating>) bundlelistOfMealRating;
       // Intent intent = getIntent();
        System.out.println(listOfMealRating);
         showList(listOfMealRating);
            }

    private void showList(ArrayList<MealRating> listOfMealRating) {

        ListView Result = findViewById(R.id.listViewResult);
        ArrayAdapter<MealRating> AdaptorList = new ArrayAdapter<>( this,android.R.layout.simple_list_item_1, listOfMealRating);
        Result.setAdapter(AdaptorList);
    }

    @Override
    public void onClick(View v) {

        String Name = editTextName.getText().toString();
        String message = " Thank you  "+ Name +"!" ;

        Intent intent = new Intent();
        intent.putExtra( "message", message);

        setResult(RESULT_OK,intent);
        finish();
    }
    public void runMe(View view){
        int selectRadio=radioGrouprate.getCheckedRadioButtonId();
        switch (selectRadio){
            case R.id.radio1:
               goStar(1,1.5f);
                break;
            case R.id.radio2:
               goStar(2,2.5f);
                break;
            case R.id.radio3:
               goStar(3,3f);
                break;
            case R.id.radioButtonSortA:
               sortAsc();
                break;
            case R.id.radioButtonSortD:
               sortDes();
                break;
        }
    }
   private void goStar(int a ,float b ){
       Iterator<MealRating> iterator = listOfMealRating.iterator();
       starFilter.clear();
       while(iterator.hasNext()){
          MealRating operation = iterator.next();
           if(operation.getRating()==a || operation.getRating()==b ){
               starFilter.add(operation);
           }
       }
       ListView ResultList = findViewById(R.id.listViewResult);
       ArrayAdapter<MealRating> listAdapter = new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, starFilter);
       ResultList.setAdapter(listAdapter);
   }

   private void sortAsc(){
       Collections.sort(listOfMealRating);
       ListView ResultList = findViewById(R.id.listViewResult);
       ArrayAdapter<MealRating> listAdapter = new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, listOfMealRating);
       ResultList.setAdapter(listAdapter);
   }
   private void sortDes(){
       Collections.sort(listOfMealRating,Collections.reverseOrder());
       ListView ResultList = findViewById(R.id.listViewResult);
       ArrayAdapter<MealRating> listAdapter = new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, listOfMealRating);
       ResultList.setAdapter(listAdapter);
   }

}