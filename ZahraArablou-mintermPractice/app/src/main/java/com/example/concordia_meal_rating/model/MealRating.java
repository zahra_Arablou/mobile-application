package com.example.concordia_meal_rating.model;
import androidx.annotation.NonNull;

import java.io.Serializable;

public class MealRating implements Comparable<MealRating>, Serializable {

    private String Name;
    private float rating;
    private String type;//salad or meal

    public MealRating(String mealName, float rating,String type) {
        this.Name = mealName;
        this.rating = rating;
        this.type=type;
    }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getMealName() {
        return Name;
    }

    public void setMealName(String mealName) {
        this.Name = mealName;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        if(type.equals("salad"))
            return "Salad: " + Name + "Rating: " + rating;
        else
            return "Meal: " + Name + "Rating: " + rating;
    }

    @Override
    public int compareTo(@NonNull MealRating mealRating) {
        return Name.compareTo(mealRating.getMealName());
    }
}
