package com.example.concordia_meal_rating;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.concordia_meal_rating.model.MealRating;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    final static int REQUEST_CODE1 = 1;
    Button btnAdd, btnShowAll,btnMeal,btnSalad;
    ImageView imageViewMeal;
    TextView title;

    Spinner spinnerMeal;
    ArrayAdapter<String> mealAdapter;
    ArrayAdapter<String> saladAdapter;

    boolean isSalad=false;

    RatingBar ratingBarMeal;

    ArrayList<MealRating> listOfMealRating;

    // Arrays of names and pictures for spinner
    String listMeal[] = {"Salmon", "Poutine", "Sushi", "Tacos"};
    int mealPicture[] = {R.drawable.salmon, R.drawable.poutine, R.drawable.sushi, R.drawable.tacos};

    String listSalad[] = {"Chicken Salad", "Montreal", "Green Salas"};
    int saladPicture[] = {R.drawable.chicken_salad, R.drawable.montreal, R.drawable.green_salad};
    //.........................................

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
            }
    private void initialize() {

        title=findViewById(R.id.title);
        listOfMealRating = new ArrayList<>();

        // Reference to ratingBar.................................
        ratingBarMeal = findViewById(R.id.ratingBar);
        //........................................................

        imageViewMeal = findViewById(R.id.imageView);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        btnMeal = findViewById(R.id.btnMeal);
        btnMeal.setOnClickListener(this);

        btnSalad = findViewById(R.id.btnSalad);
        btnSalad.setOnClickListener(this);

        // Initialize spinner -----------------------------------
        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);

      if(!isSalad) {
        mealAdapter();
      }

        //-------------------------------------------------------
    }
    private void mealAdapter()
    {
        mealAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                listMeal);
        spinnerMeal.setAdapter(mealAdapter);
    }

    private void saladAdapter(){
        saladAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                listSalad);
        spinnerMeal.setAdapter(saladAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnAdd:
                addRating();
                break;

            case R.id.btnShowAll:
                showAllMealRating();
                break;

            case R.id.btnMeal:
                showMeal();
                break;

            case R.id.btnSalad:
                showSalad();
                break;
        }
    }

    private void addRating() {

    String meal = spinnerMeal.getSelectedItem().toString();
    // Read ratingBar ....................................
    float rating = (float) ratingBarMeal.getRating();
    String type="";
    if(isSalad)
        type="salad";
    else type="Meal";
    //....................................................
    // Create new object and add it to our model array....
    MealRating mealRating = new MealRating(meal, rating,type);
    listOfMealRating.add(mealRating);
    //....................................................
        // Reset rating bar for next time
        ratingBarMeal.setRating(0);
    }
    private void showAllMealRating() {

        //Collections.sort(listOfMealRating);

        Bundle bundle = new Bundle();
        bundle.putSerializable("bundle" , listOfMealRating);

        Intent intent = new Intent( this, rating_result.class);
        intent.putExtra( "intent" , bundle);

        startActivityForResult(intent,REQUEST_CODE1);
    }
    private void showMeal(){
        isSalad=false;
        mealAdapter();
    }
    private void showSalad(){
       isSalad=true;
       saladAdapter();
    }
    // AdapterView.OnItemSelectedListener ----------------------------------------------------------
    // Callback method to be invoked when an item in this view has been selected.
    // This callback is invoked only when the newly selected position is different
    // from the previously selected position or if there was no selected item.
    @Override
    public void onItemSelected(AdapterView<?> parent, // The AdapterView where the selection happened
                               View view,             // The view within the AdapterView that was clicked
                               int i,                 // The position of the view in the ListView adapter start from zero
                               long id) {             // The row id of the item in underlying data model that is selected
        // 'i' is index of selected item in spinner,
        // so we can assign the corresponding image reference
        // from our image array to our imageView
        if(isSalad){
            int image = saladPicture[i];
            imageViewMeal.setImageResource(image);
        }else {
            int image = mealPicture[i];
            imageViewMeal.setImageResource(image);
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE1){
            String receiveData =(String) data.getStringExtra("message");
            if(resultCode == RESULT_OK){
                title.setText(receiveData);
            }
        }
    }
}

