package com.example.zahra;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.zahra.model.Account;
import com.example.zahra.model.Customer;
import java.util.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.text.DecimalFormat;

import android.os.Bundle;

public class Detail extends AppCompatActivity implements View.OnClickListener{

    EditText accoutNo,year,month,day,balance,name,family,phone,sin;
    Button add,find,remove,update,save,load,clear,showAll;
    String selectedPosition;
    Customer selectedCustomer;
    ArrayList<Customer> customerList;
    ArrayList<Account> accountList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initialize();
        getInfoFromMain();
    }

    private void initialize(){
        accoutNo=findViewById(R.id.accountNo);
        year=findViewById(R.id.year);
        month=findViewById(R.id.month);
        day=findViewById(R.id.day);
        balance=findViewById(R.id.balance);
        name=findViewById(R.id.name);
        family=findViewById(R.id.family);
        phone=findViewById(R.id.phone);
        sin=findViewById(R.id.sin);
        accountList=new ArrayList<Account>();

        add=findViewById(R.id.add);
        add.setOnClickListener(this);

        find=findViewById(R.id.find);
        find.setOnClickListener(this);

        remove=findViewById(R.id.remove);
        remove.setOnClickListener(this);

        update=findViewById(R.id.update);
        update.setOnClickListener(this);

        save=findViewById(R.id.save);
        save.setOnClickListener(this);

        load=findViewById(R.id.load);
        load.setOnClickListener(this);

        clear=findViewById(R.id.clear);
        clear.setOnClickListener(this);

        showAll=findViewById(R.id.showAll);
        showAll.setOnClickListener(this);

    }

    private void getInfoFromMain(){

        customerList=(ArrayList<Customer>)getIntent().getBundleExtra("extraIntent").getSerializable("customerList");
        selectedPosition=(String)getIntent().getBundleExtra("extraIntent").getString("SelectedPositon");
        if(selectedPosition!=null){
            int select=Integer.parseInt(selectedPosition);
            selectedCustomer=customerList.get(select);

            accoutNo.setTextKeepState((String)selectedCustomer.getAccount().getAccountNo());
            System.out.println(selectedCustomer.getAccount().getOpenDate().getYear());
            int year1=(Integer)selectedCustomer.getAccount().getOpenDate().getYear();
            year.setText(String.valueOf(year1));
            month.setText(String.valueOf(selectedCustomer.getAccount().getOpenDate().getMonth()));
            day.setText(String.valueOf(selectedCustomer.getAccount().getOpenDate().getDay()));

            double d=selectedCustomer.getAccount().getBalance();
            DecimalFormat df = new DecimalFormat("###.##");
            balance.setText(String.valueOf(df.format(d)));

            //balance.setText(String.valueOf(Double.valueOf(selectedCustomer.getAccount().getBalance()).toString()));
            name.setText(selectedCustomer.getName());
            family.setText(selectedCustomer.getFamily());
            phone.setText(selectedCustomer.getPhone());
            sin.setText(selectedCustomer.getSin());
        }
    }

    @Override
    public void onClick(View v) {
        int btnId=v.getId();
        switch (btnId){
            case R.id.add:
                goAdd();
                break;
            case R.id.find:
                goFind();
                break;
            case R.id.remove:
                goRemove();
                break;
            case R.id.update:
                goUpdate();
                break;
            case R.id.save:
                goSave();
                break;
            case R.id.load:
                goLoad();
                break;
            case R.id.clear:
                goClear();
                break;
            case R.id.showAll:
                goShowAll();
                break;
        }
    }

    private void goAdd() {
        try {
            System.out.println(accountList);
            Customer customer;
            String customerName = name.getText().toString();
            String customerFamily = family.getText().toString();
            String customerPhone = phone.getText().toString();
            String customerSin = sin.getText().toString();

            String accNo = accoutNo.getText().toString();
            Iterator<Customer> iterator=customerList.iterator();
            while (iterator.hasNext()){
                Customer customer1=iterator.next();
                if(customer1.getAccount().getAccountNo().equals(accNo)){
                    Toast.makeText(this,"Account Number already Exists!",Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            Double balanc = Double.parseDouble(balance.getText().toString());

            int openYear = Integer.parseInt(year.getText().toString());
            int openMonth = Integer.parseInt(month.getText().toString());
            int openDay = Integer.parseInt(month.getText().toString());

            Date openDate = new Date(openYear, openMonth, openDay);
            Account account = new Account(accNo, openDate, balanc);
            customer = new Customer(customerName, customerFamily, customerPhone, customerSin, account);
            customerList.add(customer);

            Toast.makeText(this, "New Customer Successfully Added!", Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
            Toast.makeText(this,"Empty fields are not allowed!",Toast.LENGTH_SHORT).show();
        }}

    private void goFind(){
        String sinNo = sin.getText().toString();
        Iterator<Customer> iterator=customerList.iterator();
        while (iterator.hasNext()){
            Customer customer1=iterator.next();
            if(customer1.getSin().equals(sinNo)){
                accoutNo.setTextKeepState((String)customer1.getAccount().getAccountNo());
                int year1=(Integer)customer1.getAccount().getOpenDate().getYear();
                year.setText(String.valueOf(year1));
                month.setText(String.valueOf(customer1.getAccount().getOpenDate().getMonth()));
                day.setText(String.valueOf(customer1.getAccount().getOpenDate().getDay()));
                balance.setText(String.valueOf(Double.valueOf(customer1.getAccount().getBalance()).toString()));
                name.setText(customer1.getName());
                family.setText(customer1.getFamily());
                phone.setText(customer1.getPhone());
                sin.setText(customer1.getSin());
            }
        }
    }

    private void goRemove(){
        String sinNo = sin.getText().toString();
        Iterator<Customer> iterator=customerList.iterator();
        while (iterator.hasNext()){
            Customer customer1=iterator.next();
            if(customer1.getSin().equals(sinNo)) {
                iterator.remove();
                Toast.makeText(this,"Customer Successfully removed!",Toast.LENGTH_SHORT).show();
                goClear();
            }
        }}

    private void goUpdate(){
        try {
            String sinNo = sin.getText().toString();
            Iterator<Customer> iterator = customerList.iterator();
            while (iterator.hasNext()) {
                Customer customer1 = iterator.next();
                if (customer1.getSin().equals(sinNo)) {
                    String customerName = name.getText().toString();
                    String customerFamily = family.getText().toString();
                    String customerPhone = phone.getText().toString();
                    String customerSin = sin.getText().toString();

                    String accNo = accoutNo.getText().toString();
                    Iterator<Customer> iterator1 = customerList.iterator();
                    while (iterator.hasNext()) {
                        Customer customer2 = iterator.next();
                        if(customer1!=customer2)
                            if (customer2.getAccount().getAccountNo().equals(accNo)) {
                                Toast.makeText(this, "Account Number already Exists!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                    }

                    Double balanc = Double.parseDouble(balance.getText().toString());

                    int openYear = Integer.parseInt(year.getText().toString());
                    int openMonth = Integer.parseInt(month.getText().toString());
                    int openDay = Integer.parseInt(month.getText().toString());

                    Date openDate = new Date(openYear, openMonth, openDay);
                    Account account = new Account(accNo, openDate, balanc);

                    customer1.setAccount(account);
                    customer1.setName(customerName);
                    customer1.setFamily(customerFamily);
                    customer1.setPhone(customerPhone);
                    customer1.setSin(customerSin);
                    System.out.println(customer1);

                    Toast.makeText(this, " Customer Successfully Updated!", Toast.LENGTH_SHORT).show();
                }
            }
        } catch(Exception e){
            Toast.makeText(this,"empty fields are not allowed!",Toast.LENGTH_SHORT).show();}
    }

    private void goClear(){
        accoutNo.setText(null);
        year.setText(null);
        month.setText(null);
        day.setText(null);
        balance.setText(null);
        name.setText(null);
        family.setText(null);
        phone.setText(null);
        sin.setText(null);
    }

    private void goShowAll(){

        Bundle bundle=new Bundle();
        bundle.putSerializable("addedcustomer",customerList);

        Intent returnIntent=new Intent();
        returnIntent.putExtra("intentReturn",bundle);
        setResult(RESULT_OK,returnIntent);

        finish();
    }

    private void goSave(){ }

    private void goLoad(){ }

}