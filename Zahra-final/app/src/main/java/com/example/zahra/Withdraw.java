package com.example.zahra;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Date;
import com.example.zahra.model.Account;
import com.example.zahra.model.Customer;

public class Withdraw extends AppCompatActivity implements View.OnClickListener {

    Button withraw;
    EditText amount;
    TextView balance;
    Customer customer;
    int selectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        initialize();
    }

    private void initialize(){
        withraw=findViewById(R.id.btnWithdraw);
        withraw.setOnClickListener(this);

        balance=findViewById(R.id.balance);
        amount=findViewById(R.id.amount);

        customer=(Customer)getIntent().getBundleExtra("extraIntent").getSerializable("customerBundle");
        selectedPosition=(int)getIntent().getBundleExtra("extraIntent").getInt("SelectedPositon");

         balance.setText(String.valueOf(customer.getAccount().getBalance()));
    }


    @Override
    public void onClick(View v) {

        Double newAmount=Double.parseDouble(amount.getText().toString());
        newAmount=customer.getAccount().getBalance()-newAmount;

        DecimalFormat df = new DecimalFormat("####.##");
        balance.setText(String.valueOf(df.format(newAmount)));

        String accNo=customer.getAccount().getAccountNo();
        Date date=customer.getAccount().getOpenDate();
        Account account=new Account(accNo,date,newAmount);
        customer.setAccount(account);
        Toast.makeText(this,"The changed obj goes to main by Back pressed. ",Toast.LENGTH_LONG).show();
      }

    @Override
    public void onBackPressed() {

        Bundle bundle=new Bundle();
        bundle.putSerializable("personObjectEdited",customer);
        bundle.putInt("selectedPosition",selectedPosition);

        Intent returnIntent=new Intent();
        returnIntent.putExtra("intentReturn",bundle);
        setResult(RESULT_OK,returnIntent);

        finish();
    }
}