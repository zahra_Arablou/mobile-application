package com.example.zahra;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentUris;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;

import java.util.ArrayList;
import com.example.zahra.model.Customer;
import com.example.zahra.model.Account;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener,View.OnClickListener {

    static final int ADD_CUSTOMER =1 ;
    static final int WITH_BALANCE =2 ;
    ListView listViewCustomer;
    Button btnCreate;
    ArrayList<Customer> customerList;
    ArrayAdapter<Customer> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDummyCustomer();
        initialize();
    }

    private void initializeDummyCustomer(){
        customerList=new ArrayList<Customer>();

        customerList.add(new Customer("Sophia","Loren","5145661614","1",new Account("12",new Date(2018,01,02),500.12)));
        customerList.add(new Customer("Nicole","Kidman","51454566614","2",new Account("14",new Date(2008,10,10),200.1)));
        customerList.add(new Customer("Diana","Spencer","514534664","3",new Account("15",new Date(2010,11,11),100)));
    }

    private void initialize(){
        listViewCustomer=findViewById(R.id.listViewCstomer);
        listViewCustomer.setOnItemClickListener(this);
        listViewCustomer.setOnItemLongClickListener(this);

        btnCreate=findViewById(R.id.create);
        btnCreate.setOnClickListener(this);
        Collections.sort(customerList);
        arrayAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,customerList);
        listViewCustomer.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle=new Bundle();

        bundle.putSerializable("customerList",(Serializable) customerList);
        String position1=String.valueOf(position);
        bundle.putString("SelectedPositon",position1);

        Intent myIntent=new Intent(this,Detail.class);
        myIntent.putExtra("extraIntent",bundle);

        startActivityForResult(myIntent,ADD_CUSTOMER);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle=new Bundle();
        bundle.putSerializable("customerBundle",(Serializable) customerList.get(position));
        bundle.putInt("SelectedPositon",position);

        Intent intent=new Intent(this,Withdraw.class);
        intent.putExtra("extraIntent",bundle);

        startActivityForResult(intent,WITH_BALANCE);
        return true;
    }

    @Override
    public void onClick(View v) {
        Bundle bundle=new Bundle();
        bundle.putSerializable("customerList",customerList);

        Intent myIntent=new Intent(this,Detail.class);
        myIntent.putExtra("extraIntent",bundle);

        startActivityForResult(myIntent,ADD_CUSTOMER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_CUSTOMER){
            if(resultCode==RESULT_OK){

                customerList= (ArrayList<Customer>) data.getBundleExtra("intentReturn").getSerializable("addedcustomer");
                Collections.sort(customerList);

                arrayAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,customerList);
                listViewCustomer.setAdapter(arrayAdapter);
         }
        }
        if(requestCode==WITH_BALANCE){
            if(resultCode == RESULT_OK){
                Customer customer=(Customer) data.getBundleExtra("intentReturn").getSerializable("personObjectEdited");
                int selectedPosition=(int)data.getBundleExtra("intentReturn").getInt("selectedPosition");

                customerList.set(selectedPosition,customer);
                System.out.println(customerList);

                arrayAdapter.notifyDataSetChanged();
            }
        }
    }
}