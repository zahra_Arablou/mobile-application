package com.example.zahra.model;


import java.io.Serializable;
import java.util.Date;

public class Account implements  Serializable {
    private String accountNo;
    private Date openDate;
    private double balance;

    public Account(String accountNo, Date openDate, double balance) {
        this.accountNo = accountNo;
        this.openDate = openDate;
        this.balance = balance;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNo='" + accountNo + '\'' +
                ", openDate=" + openDate +
                ", balance=" + balance +
                '}';
    }
}
