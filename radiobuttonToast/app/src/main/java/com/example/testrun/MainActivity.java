package com.example.testrun;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    RadioGroup radioGroupSex , radioGroupSport;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }
    private void initialize(){
        editText=findViewById(R.id.editText1);
        //**********create a refrence to Radio Groups
        radioGroupSex=findViewById(R.id.radioGroupSex);
        radioGroupSport=findViewById(R.id.radioGroupSport);
      }
    public void showMe(View view){
        //**********read editText value
        String name=editText.getText().toString();
        //********Read Radio Group value
        int selected_sex_radio_btn=radioGroupSex.getCheckedRadioButtonId();
        int selected_sport_radio_btn=radioGroupSport.getCheckedRadioButtonId();
        //*************************Sex
        String strSex;
        if (selected_sex_radio_btn==R.id.female)
            strSex="Female";
        else
            strSex="Male";
        //*******************************Sport
        String strSport=null;
        switch(selected_sport_radio_btn){
            case R.id.soccer:
                strSport="Soccer";
                break;
            case R.id.hockey:
                strSport="Hockey";
                break;
            case R.id.handball:
                strSport="Handball";
                break;
        }
        String strToToast="My name is :  "+ name +"\n"+ "I am :  "+ strSex+
                "\n"+"My favourite sport is: "+ strSport ;
        Toast.makeText(this,strToToast,Toast.LENGTH_LONG).show();
    }
}