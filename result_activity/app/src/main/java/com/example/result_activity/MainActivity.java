package com.example.result_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;
import android.content.Intent;
import androidx.annotation.Nullable;

public class MainActivity extends AppCompatActivity {
static final int ARRAY_SEND_REQUEST=1;
static final int TEXT_SEND_REQUEST=2;

    private  int counter=1;
    List<Person> listPerson;

    Button buttonSendObject;
    Button buttonSendText;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        passData();
    }
   private void initialize()
   {
       listPerson=new ArrayList<>();
       listPerson.add(new Person("ram","Montreal"));
       listPerson.add(new Person("mery","Toronto"));
       listPerson.add(new Person("ram","Montreal"));

   }
   private void passData(){
          textViewResult=findViewById(R.id.textViewResult);
          buttonSendText=findViewById(R.id.buttonSendObject);
          buttonSendText.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Bundle bundle = new Bundle();
                  bundle.putSerializable("personBundle",(Serializable) listPerson);

                  Intent myIntent = new Intent(MainActivity.this, FirstResultActivity.class);
                  myIntent.putExtra("extraIntent",bundle);
//                  startActivity(myIntent); ********we donot except any result
                 startActivityForResult(myIntent,ARRAY_SEND_REQUEST);

              }
          });

   }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String reply =
                        data.getStringExtra(FirstResultActivity.EXTRA_RETURN_MESSAG);
                textViewResult.setText(reply);
            }
        }
    }



}