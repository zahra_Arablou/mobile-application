package com.example.result_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import  java.util.List;
import java.util.ArrayList;
import android.view.View;
import android.content.Intent;

public class FirstResultActivity extends AppCompatActivity {
   static final String EXTRA_RETURN_MESSAG="";
   ListView listViewResult;
   Button  buttonRetrun;
   ArrayList<Person>personList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_result);
        getDataFromMain();
        buttonRetrun=findViewById(R.id.buttonReturn);
        buttonRetrun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent =new Intent();
                returnIntent.putExtra(EXTRA_RETURN_MESSAG,"I am from First Result");
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });
    }
    private void getDataFromMain(){
        Bundle bundle=getIntent().getBundleExtra("extraIntent");
        personList=(ArrayList<Person>)bundle.getSerializable("personBundle");

        listViewResult=findViewById((R.id.listViewResult));

        ArrayAdapter<Person> arrayAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,personList);
       listViewResult.setAdapter(arrayAdapter);
    }
}