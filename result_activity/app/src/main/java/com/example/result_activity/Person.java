package com.example.result_activity;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Person implements Serializable {
    private String Name;
    private String Address;

    public Person(String name, String address) {
        Name = name;
        Address = address;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getName() {
        return Name;
    }

    public String getAddress() {
        return Address;
    }

    @NonNull
    @Override
    public String toString() {
        return "Person{"+
                "Name='"+Name+'\''+
                ", Address='"+Address+'\''+
                '}';
    }
}
