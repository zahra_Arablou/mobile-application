package com.example.radiobutton_image;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    RadioGroup geroupButton;
    ImageView img1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       initialize();
    }

    private void initialize(){
        geroupButton =findViewById(R.id.gr);
        img1=findViewById(R.id.img);
    }
    public void runMe(View view){


        int selectRadio=geroupButton.getCheckedRadioButtonId();
        switch (selectRadio){
            case R.id.bird:
                img1.setImageResource(R.drawable.bird);
                break;
            case R.id.cat:
                img1.setImageResource(R.drawable.cat);
                break;
            case R.id.dog:
                img1.setImageResource(R.drawable.dog);
                break;
        }
    }
}