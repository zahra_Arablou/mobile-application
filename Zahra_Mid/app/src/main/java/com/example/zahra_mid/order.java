package com.example.zahra_mid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.zahra_mid.model.Order;
import java.io.Serializable;
import android.widget.Toast;

public class order extends AppCompatActivity implements View.OnClickListener{
    TextView customerName,order;
    EditText address;
    Button btnBack;
    Order myOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        initialize();
    }
    private void initialize(){
        customerName=findViewById(R.id.customerName);
        order=findViewById(R.id.order);
        address=findViewById(R.id.address);

        btnBack=findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        Bundle bundle = getIntent().getBundleExtra("intent");
        Serializable bundledOrder = bundle.getSerializable("bundle");

        myOrder = (Order) bundledOrder;

        Intent intent = getIntent();
        customerName.setText(myOrder.getName());
        order.setText(myOrder.getMeal());
    }

    @Override
    public void onClick(View v) {

        String name=myOrder.getName();
         String toast ="Thank you for using our application "+ name  ;
        Toast.makeText(this, toast.toString(), Toast.LENGTH_SHORT).show();

        String message="Address is:  "+address.getText().toString();

        Intent intent = new Intent();
        intent.putExtra( "message", message);

        setResult(RESULT_OK,intent);
        finish();
    }
}