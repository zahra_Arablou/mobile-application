package com.example.zahra_mid;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.example.zahra_mid.model.Order;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final static int REQUEST_CODE1 = 1;
    RadioGroup radioGroup;
    EditText name;
    TextView thank;
    Button order,finish;
    ImageView img;
    String personName,orderedFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

private void initialize(){

        radioGroup=findViewById(R.id.radioGroup);
        name=findViewById(R.id.Name);
        thank=findViewById(R.id.thank);
        img=findViewById(R.id.imgFood);

        order=findViewById(R.id.btnOrder);
        order.setOnClickListener(this);

        finish=findViewById(R.id.btnFinish);
        finish.setOnClickListener(this);
}
public void runMe(View v){
        int btn=radioGroup.getCheckedRadioButtonId();
        switch (btn){
            case R.id.poutine:
                img.setImageResource(R.drawable.poutine);
                orderedFood="Poutine";
                break;
            case R.id.ch_poutine:
                img.setImageResource(R.drawable.chef_poutine);
                orderedFood="chef Poutine";
                break;
            case R.id.salmon:
                img.setImageResource(R.drawable.salmon);
                orderedFood="Salmon";
                break;
            case R.id.soushi:
                img.setImageResource(R.drawable.sushi);
                orderedFood="Sushi";
                break;
            case R.id.tacos:
                img.setImageResource(R.drawable.tacos);
                orderedFood="Tacos";
                break;
        }
}

    @Override
 public void onClick(View v) {
int btn=v.getId();
switch(btn){
    case R.id.btnOrder:
        goOrder();
        break;
    case R.id.btnFinish:
        finish();
        break;
}
    }

    private void goOrder(){
        personName=name.getText().toString();
        Order myOrder=new Order(personName,orderedFood);

        Bundle bundle = new Bundle();
        bundle.putSerializable("bundle" ,myOrder);

        Intent intent = new Intent( this, order.class);
        intent.putExtra( "intent" , bundle);

        startActivityForResult(intent,REQUEST_CODE1);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE1){
            String receiveData =(String) data.getStringExtra("message");
            if(resultCode == RESULT_OK){
                thank.setText(receiveData);
            }
        }
    }
}