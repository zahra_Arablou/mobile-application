package com.example.zahra_mid.model;

import java.io.Serializable;

public class Order implements Serializable {
    private String name;
    private String meal;

    public Order(String name, String meal) {
        this.name = name;
        this.meal = meal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    @Override
    public String toString() {
        return "order{" +
                "name='" + name + '\'' +
                ", meal='" + meal + '\'' +
                '}';
    }
}
